<!--  Copyright (C) Intel Corp.  2014.  All Rights Reserved. -->

<!--  Permission is hereby granted, free of charge, to any person obtaining -->
<!--  a copy of this software and associated documentation files (the -->
<!--  "Software"), to deal in the Software without restriction, including -->
<!--  without limitation the rights to use, copy, modify, merge, publish, -->
<!--  distribute, sublicense, and/or sell copies of the Software, and to -->
<!--  permit persons to whom the Software is furnished to do so, subject to -->
<!--  the following conditions: -->

<!--  The above copyright notice and this permission notice (including the -->
<!--  next paragraph) shall be included in all copies or substantial -->
<!--  portions of the Software. -->

<!--  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, -->
<!--  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF -->
<!--  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. -->
<!--  IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE -->
<!--  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION -->
<!--  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION -->
<!--  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. -->

<!--   **********************************************************************/ -->
<!--   * Authors: -->
<!--   *   Mark Janes <mark.a.janes@intel.com> -->
<!--   **********************************************************************/ -->

<build_specification>
  <build_master host="mesa-ci-jenkins.jf.intel.com"
		results_dir="/mnt/jenkins/results"/>
  <git_server host="otc-mesa-ci.jf.intel.com"/>
  <results_server host="otc-mesa-ci.jf.intel.com"
		  results_dir="/mnt/jenkins/results"/>

  <!-- specified the dependency relationships between projects -->
  <projects>
    
    <!-- each project has a matching subdirectory with a build.py
         which automates the build.  -->
    <project name="mesa_ci"/>

    <project name="sim-drm"/>

    <project name="fulsim">
      <prerequisite name="sim-drm" hardware="builder" arch="m64"/>
    </project>

    <project name="drm"/>

    <project name="mesa">
      <!-- options for a prerequisite are inherited unless overridden.
           For example, a build of mesa with arch=m32 will require a
           build of drm with arch=m32 -->
      <prerequisite name="drm"/>
    </project>

    <!-- only works on m64, due to llvm dependencies -->
    <project name="mesa-buildtest" src_dir="mesa">
      <prerequisite name="drm"/>
    </project>

    <project name="waffle">
      <prerequisite name="drm"/>
    </project>

    <!-- test projects should specify bisect_hardware and bisect_arch,
         which designate the full set of platforms that should be
         re-tested when a regression occurs.  See update_conf.py and
         bisect_project.py in scripts. -->
    <project name="deqp-test" src_dir="deqp"
      bisect_hardware="bsw,bdw,gen9,gen9_zink,icl,icl_zink,tgl,tgl_zink,gen9atom,dg2,dg2_zink,mtl,mtl_zink">
      <!-- deqp-test will be specificed for a test platform (eg skl),
           but the hardware for its prerequisites should be a builder
           (since they are all compilation projects) -->
      <prerequisite name="deqp" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
    </project>

    <project name="deqp-runtime">
      <prerequisite name="deqp" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
    </project>

    <project name="deqp">
      <prerequisite name="mesa"/>
    </project>

    <project name="glescts" src_dir="cts">
      <prerequisite name="mesa"/>
    </project>

    <project name="glcts">
      <prerequisite name="mesa"/>
    </project>

    <project name="glcts-test"
             bisect_hardware="bsw,gen9,icl,bdw,tgl,dg2,mtl">
      <prerequisite name="glcts" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
    </project>

    <project name="glescts-test"
             bisect_hardware="bsw,bdw,gen9,icl,gen9atom,tgl,dg2,mtl">
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="glescts" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
    </project>

    <project name="mi-builder-test">
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
    </project>

    <project name="glescts-full">
      <prerequisite name="glescts-test" hardware="bdw,gen9,icl,tgl,dg2,mtl"/>
      <!-- only_for_type enforces the prerequisite when type matches
           the value. -->
      <prerequisite name="glescts-test" hardware="bsw,gen9atom" only_for_type="daily"/>
      <prerequisite name="glescts-test" hardware="lnl" arch="m64" shard="2"/>
      <prerequisite name="glescts-test"
                    only_for_type="daily"
                    arch="m32"
                    hardware="bdw,gen9,icl,tgl,dg2,mtl"/>
    </project>

    <project name="vkrunner"/>
      
    <project name="piglit">
      <prerequisite name="waffle" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="vkrunner" hardware="builder"/>
    </project>

    <project name="piglit-test"
             bisect_hardware="bsw,gen9,gen9_zink,gen9atom,bdw,icl,icl_zink,tgl,tgl_zink,dg2,dg2_zink,mtl,mtl_zink">
      <prerequisite name="piglit" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="sim-drm" hardware="builder" only_for_hardware="tgl_sim"/>
      <prerequisite name="fulsim" only_for_hardware="tgl_sim"/>
    </project>

    <project name="piglit-full">
      <prerequisite name="piglit-test"
                    hardware="bdw,gen9,icl,tgl,tgl_zink,dg2,dg2_zink,mtl,mtl_zink"/>
      <prerequisite name="piglit-test"
                    hardware="gen9_zink,icl_zink"
		    shard="2"/>
      <prerequisite name="piglit-test"
                    only_for_type="daily"
                    hardware="gen9,icl,tgl,dg2,mtl"
                    arch="m32"
                    shard="4"/>
      <prerequisite only_for_type="daily" name="piglit-test" hardware="gen9atom"
                    arch="m64,m32" shard="4"/>
      <prerequisite name="piglit-test" hardware="lnl"
                    arch="m64" shard="8"/>
      <prerequisite only_for_type="daily" name="piglit-test" hardware="bsw" shard="6"/>
    </project>
    
    <project name="piglit-zink"
	     bisect_hardware="gen9_zink,icl_zink,tgl_zink,dg2_zink,mtl_zink">
    </project>

    <project name="crucible">
      <prerequisite name="mesa"/>
      <prerequisite name="glslang"/>
    </project>

    <project name="glslang"/>

    <project name="crucible-test"
             bisect_hardware="bdw,bsw,gen9atom,gen9,icl,tgl,dg2,mtl">
      <prerequisite name="crucible" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
      <prerequisite name="sim-drm" hardware="builder" only_for_hardware="tgl_sim"/>
      <prerequisite name="fulsim" only_for_hardware="tgl_sim"/>
    </project>

    <project name="crucible-full">
      <prerequisite name="crucible-test" hardware="bdw,gen9atom,gen9,icl,tgl,dg2,mtl"/>
      <prerequisite name="crucible-test" only_for_type="daily" arch="m64" hardware="gen9atom,bsw"/>
      <prerequisite name="crucible-test" hardware="lnl" arch="m64" shard="2"/>
    </project>

    <project name="vulkancts-test" src_dir="vulkancts"
             bisect_hardware="bdw,bsw,gen9atom,gen9,icl,tgl,dg2,mtl">
      <prerequisite name="vulkancts" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
      <prerequisite name="sim-drm" hardware="builder" only_for_hardware="tgl_sim"/>
      <prerequisite name="fulsim" only_for_hardware="tgl_sim"/>
    </project>

    <project name="vulkancts-full">
      <prerequisite name="vulkancts-test" hardware="tgl,dg2" shard="4"/>
      <prerequisite name="vulkancts-test" hardware="bdw,gen9,icl,mtl" shard="6"/>
      <prerequisite name="vulkancts-test"
                    only_for_type="daily"
                    hardware="bdw,gen9,icl,tgl,dg2,mtl"
                    arch="m32"
                    shard="6"/>
      <prerequisite name="vulkancts-test"
                    only_for_type="daily"
                    hardware="bsw,gen9atom"
                    arch="m64"
                    shard="20"/>
      <prerequisite name="vulkancts-test" hardware="lnl" arch="m64" shard="8"/>
      <prerequisite name="crucible-full"/>
    </project>

    <project name="mi-builder-full">
      <prerequisite name="mi-builder-test" hardware="icl,tgl"/>
    </project>

    <project name="vulkancts"/>

    <project name="all-test-vulkan">
      <prerequisite name="all-test"/>
      <prerequisite name="vulkancts-full"/>
    </project>

    <project name="test-vulkan">
      <prerequisite name="mesa-buildtest" hardware="builder" arch="m64"/>
      <prerequisite name="vulkancts-full"/>
      <prerequisite name="crucible-full"/>
    </project>

    <!-- all-test has no build.py, and exists only to provide
         dependency relationships. -->
    <project name="all-test">
      <prerequisite name="piglit-full"/>
      <prerequisite name="deqp-full"/>
      <prerequisite name="mesa-buildtest" hardware="builder" arch="m64"/>
      <prerequisite name="glcts-full"/>
      <prerequisite name="glescts-full"/>
      <prerequisite name="mi-builder-full"/>
    </project>

    <project name="test-single-arch-simdrm">
      <prerequisite name="piglit-test" hardware="tgl_sim" arch="m64" shard="3"/>
      <prerequisite name="crucible-test" hardware="tgl_sim" arch="m64"/>
      <prerequisite name="vulkancts-test" hardware="tgl_sim" arch="m64" shard="4"/>
    </project>

    <!-- For testing internal mesa branch -->
    <project name="test-single-arch-internal">
      <prerequisite name="mesa-buildtest" hardware="builder" arch="m64"/>
      <prerequisite name="test-single-arch-gen9"/>
      <prerequisite name="test-single-arch-tgl"/>
      <prerequisite name="test-single-arch-dg2"/>
      <prerequisite name="test-single-arch-mtl"/>
    </project>

    <project name="test-single-arch-tgl-gl">
      <prerequisite name="piglit-test" hardware="tgl" shard="2" arch="m64"/>
      <prerequisite name="deqp-test" hardware="tgl" shard="3" arch="m64"/>
      <prerequisite name="glcts-test" hardware="tgl" arch="m64"/>
      <prerequisite name="glescts-test" hardware="tgl" arch="m64"/>
      <prerequisite name="mi-builder-test" hardware="tgl"/>
      <!-- <prerequisite name="skqp-test" hardware="tgl"/> -->
    </project>

    <project name="test-single-arch-tgl-vulkan">
      <prerequisite name="vulkancts-test" hardware="tgl" shard="8" arch="m64"/>
      <prerequisite name="crucible-test" hardware="tgl" arch="m64"/>
    </project>

    <project name="test-single-arch-tgl">
      <prerequisite name="test-single-arch-tgl-vulkan"/>
      <prerequisite name="test-single-arch-tgl-gl"/>
    </project>

    <project name="test-single-arch-tgl-sim">
        <prerequisite name="piglit-test" hardware="tgl_sim" arch="m64" shard="3"/>
        <prerequisite name="crucible-test" hardware="tgl_sim" arch="m64"/>
    </project>

    <project name="test-single-arch-mtl">
     <prerequisite name="glescts-test" hardware="mtl" arch="m64"/>
     <prerequisite name="glcts-test" hardware="mtl" arch="m64"/>
     <prerequisite name="deqp-test" hardware="mtl,mtl_zink" shard="2" arch="m64"/>
     <prerequisite name="piglit-test" hardware="mtl,mtl_zink" arch="m64"/>
     <prerequisite name="vulkancts-test" hardware="mtl" arch="m64" shard="4"/>
     <prerequisite name="crucible-test" hardware="mtl" arch="m64"/>
    </project>

    <project name="test-single-arch-gen9-gl">
      <prerequisite name="piglit-test" hardware="gen9" shard="2" arch="m64"/>
      <prerequisite name="deqp-test" hardware="gen9" shard="4" arch="m64"/>
      <prerequisite name="glcts-test" hardware="gen9" arch="m64"/>
      <prerequisite name="glescts-test" hardware="gen9" arch="m64"/>
    </project>

    <project name="test-single-arch-gen9-vulkan">
      <prerequisite name="vulkancts-test" hardware="gen9" shard="8" arch="m64"/>
      <prerequisite name="crucible-test" hardware="gen9" arch="m64"/>
    </project>

    <project name="test-single-arch-gen9">
      <prerequisite name="test-single-arch-gen9-vulkan"/>
      <prerequisite name="test-single-arch-gen9-gl"/>
    </project>

    <project name="test-single-arch-lnl">
      <prerequisite name="glescts-test" hardware="lnl" arch="m64" shard="2"/>
      <prerequisite name="glcts-test" hardware="lnl" arch="m64" shard="2"/>
      <prerequisite name="deqp-test" hardware="lnl" arch="m64"  shard="16"/>
      <prerequisite name="piglit-test" hardware="lnl" arch="m64" shard="8"/>
      <prerequisite name="vulkancts-test" hardware="lnl" arch="m64" shard="16"/>
      <prerequisite name="crucible-test" hardware="lnl" arch="m64" shard="2"/>
    </project>

    <project name="test-single-arch-lnl-sim">
      <prerequisite name="piglit-test" hardware="lnl_sim" arch="m64" shard="12"/>
      <prerequisite name="crucible-test" hardware="lnl_sim" arch="m64" shard="8"/>
      <prerequisite name="vulkancts-test" hardware="lnl_sim" arch="m64" shard="12"/>
    </project>

    <project name="test-single-arch-vulkan">
      <prerequisite name="test-single-arch"/>
      <prerequisite name="vulkancts-full"/>
    </project>

    <project name="test-single-arch">
      <prerequisite name="piglit-full"/>
      <prerequisite only_for_type="daily" name="piglit-test" hardware="gen9atom" shard="4"/>
      <prerequisite name="mesa-buildtest" hardware="builder" arch="m64"/>
      <prerequisite name="deqp-full"/>
      <prerequisite name="glcts-full"/>
      <prerequisite name="glescts-full"/>
      <prerequisite name="mi-builder-full"/>
    </project>

    <project name="test-single-arch-crocus">
      <prerequisite name="piglit-test" hardware="snb_crocus,ivb_crocus,bsw_crocus,hsw_crocus" shard="2"/>
      <prerequisite name="piglit-test"
                    only_for_type="daily"
                    hardware="ilk_crocus,byt_crocus" shard="4"/>
      <prerequisite name="deqp-test" hardware="snb_crocus,ivb_crocus,bsw_crocus,hsw_crocus" shard="4"/>
      <prerequisite name="deqp-test"
                    only_for_type="daily"
                    hardware="ilk_crocus,byt_crocus" shard="4"/>
      <prerequisite name="glcts-test" hardware="snb_crocus,ivb_crocus,bsw_crocus,hsw_crocus"/>
      <prerequisite name="glcts-test"
                    only_for_type="daily"
                    hardware="ilk_crocus,byt_crocus"/>
      <prerequisite name="glescts-test" hardware="snb_crocus,ivb_crocus,bsw_crocus,hsw_crocus"/>
      <prerequisite name="glescts-test"
                    only_for_type="daily"
                    hardware="ilk_crocus,byt_crocus"/>
    </project>

    <project name="test-single-arch-zink">
      <prerequisite name="test-single-arch"/>
      <prerequisite name="deqp-zink"/>
      <prerequisite name="piglit-zink"/>
    </project>

    <project name="deqp-full">
      <prerequisite name="deqp-test" hardware="bdw,gen9,gen9_zink" shard="4"/>
      <prerequisite name="deqp-test" hardware="icl,icl_zink,tgl,tgl_zink,dg2,dg2_zink,mtl,mtl_zink" shard="2"/>
      <prerequisite name="deqp-test"
                    only_for_type="daily"
                    arch="m32"
                    hardware="bdw,gen9,icl,tgl,dg2,mtl" shard="4"/>
      <prerequisite name="deqp-test"
                    only_for_type="daily"
                    hardware="gen9atom" shard="8" arch="m32,m64"/>
      <prerequisite name="deqp-test"
                    hardware="lnl" shard="16" arch="m64"/>
      <prerequisite name="deqp-test" only_for_type="daily" hardware="bsw" shard="6"/>
    </project>

    <project name="deqp-zink">
      <prerequisite name="deqp-test" hardware="gen9_zink" shard="4"/>
      <prerequisite name="deqp-test" hardware="icl_zink,tgl_zink,dg2_zink,mtl_zink" shard="2"/>
    </project>

    <project name="deqp-runtime-full">
      <prerequisite name="deqp-runtime" hardware="bdw,gen9" shard="5"/>
      <prerequisite name="deqp-runtime" hardware="gen9atom" shard="8"/>
    </project>

    <project name="glcts-full">
      <prerequisite name="glcts-test" hardware="bdw,gen9,icl,tgl,dg2,mtl" arch="m64"/>
      <prerequisite name="glcts-test" only_for_type="daily" hardware="bdw,icl,tgl,dg2,mtl" arch="m32"/>
      <prerequisite name="glcts-test" hardware="lnl" arch="m64" shard="2"/>
      <prerequisite name="glcts-test" only_for_type="daily" hardware="bsw"/>
    </project>

    <project name="reboot-slave"/>

    <project name="piglit-gen12">
      <prerequisite name="piglit-test" hardware="tgl" shard="2" arch="m64"/>
    </project>

    <project name="test-single-arch-dg2">
      <prerequisite name="glescts-test" hardware="dg2" arch="m64"/>
      <prerequisite name="glcts-test" hardware="dg2" arch="m64"/>
      <prerequisite name="deqp-test" hardware="dg2" shard="2" arch="m64"/>
      <prerequisite name="piglit-test" hardware="dg2" arch="m64" shard="4"/>
      <prerequisite name="vulkancts-test" hardware="dg2" arch="m64" shard="4"/>
      <prerequisite name="crucible-test" hardware="dg2" arch="m64"/>
    </project>

    <project name="linux"/>

    <project name="test-xe">
      <prerequisite name="crucible-test" hardware="xe_gen125,xe_gen12"/>
      <prerequisite name="deqp-test" hardware="xe_gen125,xe_gen12" shard="2"/>
      <prerequisite name="glcts-test" hardware="xe_gen125,xe_gen12"/>
      <prerequisite name="glescts-test" hardware="xe_gen125,xe_gen12"/>
      <prerequisite name="piglit-test" hardware="xe_gen125,xe_gen12" shard="6"/>
      <prerequisite name="vulkancts-test" hardware="xe_gen125,xe_gen12" shard="4"/>
    </project>

  </projects>

  <!-- the following servers and remotes correspond to projects -->
  <repos>
    <mesa repo="https://gitlab.freedesktop.org/mesa/mesa.git"
            branch="origin/main">
      <remote name="ci_mesa_repo" repo="https://gitlab.freedesktop.org/Mesa_CI/repos/mesa.git"/>
      <remote name="ci_internal_rebase_mesa_repo" repo="ssh://git@gitlab.devtools.intel.com:29418/mesa_ci/repos/mesa-rebase-testing.git"/>
      <remote name="internal-github" repo="git@github.com:intel-innersource/drivers.gpu.mesa.dev.git"/>
      <remote name="jljusten-checkpoint" repo="git@github.com:intel-innersource/drivers.gpu.mesa.checkpoints"/>
      <remote name="global_logic" repo="https://gitlab.freedesktop.org/GL/mesa.git"/>
      <remote name="gfxstrand" repo="https://gitlab.freedesktop.org/gfxstrand/mesa.git"/>
      <remote name="tarceri" repo="https://gitlab.freedesktop.org/tarceri/mesa.git"/>
      <remote name="zmike" repo="https://gitlab.freedesktop.org/zmike/mesa.git"/>
    </mesa>

    <drm repo="http://anongit.freedesktop.org/git/mesa/drm.git"
         branch="origin/main">
      <remote name="ci_mesa_repo" repo="https://gitlab.freedesktop.org/Mesa_CI/repos/drm.git"/>
    </drm>

    <vkrunner repo="https://github.com/Igalia/vkrunner.git" branch="fdo/main">
      <remote name="fdo" repo="https://gitlab.freedesktop.org/mesa/vkrunner.git"/>
    </vkrunner>

    <piglit repo="https://gitlab.freedesktop.org/mesa/piglit.git"
            branch="origin/main">
      <remote name="ci_piglit_repo" repo="https://gitlab.freedesktop.org/Mesa_CI/repos/piglit.git"/>
      <!-- Dylan's repository is more stable than the origin, and will
           not trigger builds unnecessarily -->
      <remote name="nchery-gitlab" repo="https://gitlab.freedesktop.org/nchery/piglit.git"/>
    </piglit>

    <waffle repo="https://gitlab.freedesktop.org/mesa/waffle.git"/>

    <!-- just so the master can cache it for the builders -->
    <mesa_jenkins repo="https://gitlab.freedesktop.org/Mesa_CI/mesa_jenkins.git">
      <remote name="ngcortes" repo="https://gitlab.freedesktop.org/ngcortes/mesa_jenkins.git"/>
      <remote name="ybogdano" repo="https://gitlab.freedesktop.org/ybogdano/mesa_jenkins.git"/>
      <remote name="majanes" repo="https://gitlab.freedesktop.org/majanes/mesa_jenkins.git"/>
      <remote name="sagarghuge" repo="https://gitlab.freedesktop.org/sagarghuge/mesa_jenkins.git"/>
    </mesa_jenkins>

    <mesa_ci repo="https://gitlab.freedesktop.org/Mesa_CI/mesa_ci.git">
      <remote name="ngcortes" repo="https://gitlab.freedesktop.org/ngcortes/mesa_ci.git"/>
      <remote name="ybogdano" repo="https://gitlab.freedesktop.org/ybogdano/mesa_ci.git"/>
      <remote name="majanes" repo="https://gitlab.freedesktop.org/majanes/mesa_ci.git"/>
      <remote name="sagarghuge" repo="https://gitlab.freedesktop.org/sagarghuge/mesa_ci.git"/>
      <remote name="jljusten" repo="https://gitlab.freedesktop.org/jljusten/mesa_ci.git"/>
    </mesa_ci>

    <deqp repo="https://android.googlesource.com/platform/external/deqp"
          branch="khronos/vulkan-cts-1.3.8">
      <remote name="khronos" repo="https://github.com/KhronosGroup/VK-GL-CTS.git"/>
      <remote name="ci" repo="https://gitlab.freedesktop.org/Mesa_CI/repos/deqp.git"/>
    </deqp>

    <cts repo="git@gitlab.khronos.org:Tracker/vk-gl-cts.git"
         branch="github/opengl-es-cts-3.2.9">
      <remote name="github" repo="https://github.com/KhronosGroup/VK-GL-CTS.git"/>
    </cts>

    <glcts repo="git@gitlab.khronos.org:Tracker/vk-gl-cts.git"
           branch="origin/opengl-cts-4.6.5">
    </glcts>

    <gtest repo="https://android.googlesource.com/platform/external/googletest"/>
    <glslang repo="https://github.com/KhronosGroup/glslang.git"
	     branch="origin/main">
      <remote name="glsl" repo="git@gitlab.khronos.org:GLSL/glslang.git"/>
    </glslang>

    <crucible repo="https://gitlab.freedesktop.org/mesa/crucible.git"
              branch="origin/main">
      <remote name="ci_crucible_repo" repo="https://gitlab.freedesktop.org/Mesa_CI/repos/crucible.git"/>
    </crucible>

    <vulkancts repo="https://github.com/KhronosGroup/VK-GL-CTS.git"
               branch="origin/vulkan-cts-1.3.10">
      <remote name="khronos" repo="git@gitlab.khronos.org:Tracker/vk-gl-cts.git"/>
      <remote name="internal-github" repo="git@github.com:intel-innersource/drivers.gpu.mesa.vkglcts.git"/>
    </vulkancts>
    
    <spirvtools repo="https://github.com/KhronosGroup/SPIRV-Tools.git"
                branch="khronos/master">
      <remote name="khronos" repo="ssh://git@gitlab.khronos.org/spirv/spirv-tools.git"/>
      <remote name="ci" repo="https://gitlab.freedesktop.org/Mesa_CI/repos/spirvtools.git"/>
    </spirvtools>
    
    <spirvheaders repo="https://github.com/KhronosGroup/SPIRV-Headers.git"
                  branch="origin/main">
      <remote name="khronos" repo="git@gitlab.khronos.org:spirv/SPIRV-Headers.git"/>
      <remote name="ci" repo="https://gitlab.freedesktop.org/Mesa_CI/repos/spirv-headers.git"/>
    </spirvheaders>

    <vk-video-parser repo="https://github.com/nvpro-samples/vk_video_samples.git"
		     branch="origin/main">
      <remote name="igalia" repo="https://github.com/Igalia/vk_video_samples.git"/>
    </vk-video-parser>

    <nvidia-video-samples repo="https://github.com/Igalia/vk_video_samples.git"
		     branch="origin/main">
    </nvidia-video-samples>

    <vulkan-validationlayers repo="https://github.com/KhronosGroup/Vulkan-ValidationLayers.git"
			     branch="origin/main">
    </vulkan-validationlayers>

    <esextractor repo="https://github.com/Igalia/ESExtractor.git"
		 branch="origin/main"/>

    <vulkandocs repo="https://github.com/KhronosGroup/Vulkan-Docs.git"
                branch="origin/main">
    </vulkandocs>

    <kc-cts repo="git@gitlab.khronos.org:opengl/kc-cts.git"/>

    <!-- repositories for private CI configuration details -->
    <mesa_ci_internal repo="git@github.com:intel-innersource/drivers.gpu.mesa.ci.internal.git"/>
    <mesa_ci_config repo="git@github.com:intel-sandbox/mesa_ci_config.git" branch="origin/main"/>
    <mesa_ci_docker repo="https://gitlab.freedesktop.org/Mesa_CI/mesa_ci_docker"/>

    <sim-drm repo="git@github.com:intel-innersource/drivers.gpu.mesa.sim-drm.git"
	     branch="origin/main">
    </sim-drm>

    <amber repo="https://github.com/google/amber.git"
	   branch="origin/main">
      <remote name="ci" repo="https://gitlab.freedesktop.org/Mesa_CI/repos/amber.git"/>
    </amber>
    <jsoncpp repo="https://github.com/open-source-parsers/jsoncpp.git"
	     branch="origin/master"/> 

    <!-- configuration for jenkins jobs -->
    <pipeline repo="https://gitlab.freedesktop.org/Mesa_CI/mesa_ci_pipelines.git"
	      branch="origin/gl_main"/>
    <pipeline_private repo="git@github.com:intel-innersource/drivers.gpu.mesa.ci.pipelines.git"
	      branch="origin/main"/>

    <!-- Xe linux KMD -->
    <linux repo="https://gitlab.freedesktop.org/Mesa_CI/repos/linux.git"
	   branch="origin/xe/intel_ci">
      <remote name="internal" repo="git@github.com:intel-sandbox/drivers.gpu.mesa.linux.git"/>
      <remote name="innersource" repo="git@github.com:intel-innersource/drivers.gpu.mesa.kernel.git"/>
    </linux>

    <perforate repo="https://gitlab.freedesktop.org/Mesa_CI/perforate.git" branch="origin/main">
      <remote name="ngcortes" repo="https://gitlab.freedesktop.org/ngcortes/perforate.git"/>
    </perforate>

  </repos>


  <branches>
    <!-- the following branches are polled continuously.  Any commit
         will trigger a branch build with an identifier based on the
         commit that triggered the build.  Any repository listed as a
         subtag of the branch can trigger a build of the branch.
         Repositories default to origin/master -->

    <!-- jenkins has a build with same name as branch -->
    <!-- multi branch pipeline builds have subjobs that are indexed by a '/' -->
    <branch pipeline="public" name="gl_main" project="all-test" priority="3">

      <!-- these repo tags exist soley to trigger a master build when
           anything changes -->
      <mesa branch="origin/main"/>
      <piglit/>
      <waffle/>
      <vulkancts/>
      <drm/>
      <vkrunner/>
      <deqp/>
      <cts branch="origin/opengl-es-cts-3.2.9"/>
      <glcts branch="origin/opengl-cts-4.6.5"/>
      <glslang/>
    </branch>

    <branch pipeline="public" name="released_22.3" project="all-test-vulkan">
      <mesa branch="origin/22.3"/>
      <crucible branch="ae7859b2df98e6da9ea131d18f4e222fe64bb4ed" />
      <cts branch="99dfdd1a5db25f59eeeceb8610ee209fd77e3250" />
      <deqp branch="2c901e676b98e32859c8057dc344d280eaa43091" />
      <drm branch="b9ca37b3134861048986b75896c0915cbf2e97f9" />
      <glcts branch="eeaa9cf081f637c87ca3ce525231ecf60e36064e" />
      <piglit branch="71eb7b6880c842da2c2414f2be9468e9fd95d525" />
      <vkrunner branch="1b4cc6b129e857d88ba9487bc8a4983d6a11df02" />
      <vulkancts branch="b71dd67ffbb62bfcd12a6f05914ced05c88c6a2c" />
      <waffle branch="692046d8ba4ac6fdd8803a5da21b2ac8b2521113" />
    </branch>

    <branch pipeline="public" name="staging_22.3" project="all-test-vulkan">
      <mesa branch="origin/staging/22.3"/>
      <crucible branch="ae7859b2df98e6da9ea131d18f4e222fe64bb4ed" />
      <cts branch="99dfdd1a5db25f59eeeceb8610ee209fd77e3250" />
      <deqp branch="2c901e676b98e32859c8057dc344d280eaa43091" />
      <drm branch="b9ca37b3134861048986b75896c0915cbf2e97f9" />
      <glcts branch="eeaa9cf081f637c87ca3ce525231ecf60e36064e" />
      <piglit branch="71eb7b6880c842da2c2414f2be9468e9fd95d525" />
      <vkrunner branch="1b4cc6b129e857d88ba9487bc8a4983d6a11df02" />
      <vulkancts branch="b71dd67ffbb62bfcd12a6f05914ced05c88c6a2c" />
      <waffle branch="692046d8ba4ac6fdd8803a5da21b2ac8b2521113" />
    </branch>

    <branch pipeline="public" name="released_23.0" project="all-test-vulkan">
      <mesa branch="origin/23.0"/>
      <crucible branch="1b0be52e91f05d109053afeb07d9e43b160c9b32" />
      <cts branch="b746f1bc91f6b7e30a1d0d059d44ad22c0d96614" />
      <deqp branch="2c901e676b98e32859c8057dc344d280eaa43091" />
      <drm branch="0e2c7d05712d65903a9b77fb9f960ddff43bac64" />
      <glcts branch="79642ee8dcafa6e92305f0ab1d8f24e8cc100ec2" />
      <piglit branch="f4539c31b1e98502e1224b5a91f8e124527e9cb7" />
      <vkrunner branch="810bfc5108cd002513e2578c23543329cc8ae5f5" />
      <vulkancts branch="b71dd67ffbb62bfcd12a6f05914ced05c88c6a2c" />
      <waffle branch="692046d8ba4ac6fdd8803a5da21b2ac8b2521113" />
    </branch>

    <branch pipeline="public" name="staging_23.0" project="all-test-vulkan" priority="4">
      <mesa branch="origin/staging/23.0"/>
      <crucible branch="1b0be52e91f05d109053afeb07d9e43b160c9b32" />
      <cts branch="b746f1bc91f6b7e30a1d0d059d44ad22c0d96614" />
      <deqp branch="2c901e676b98e32859c8057dc344d280eaa43091" />
      <drm branch="0e2c7d05712d65903a9b77fb9f960ddff43bac64" />
      <glcts branch="79642ee8dcafa6e92305f0ab1d8f24e8cc100ec2" />
      <piglit branch="f4539c31b1e98502e1224b5a91f8e124527e9cb7" />
      <vkrunner branch="810bfc5108cd002513e2578c23543329cc8ae5f5" />
      <vulkancts branch="b71dd67ffbb62bfcd12a6f05914ced05c88c6a2c" />
      <waffle branch="692046d8ba4ac6fdd8803a5da21b2ac8b2521113" />
    </branch>

    <branch pipeline="public" name="vulkan_main" project="vulkancts-full" priority="3">
      <mesa branch="origin/main"/>
      <vulkancts/>
      <crucible/>
      <glslang/>
      <spirvheaders branch="801cca8104245c07e8cc53292da87ee1b76946fe"/>
      <spirvtools/>
      <glslang/>
    </branch>

    <branch pipeline="public" name="dev_aswarup" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/aswarup"/>
    </branch>

    <branch name="jljusten_internal" project="test-single-arch" priority="2"/>

    <branch pipeline="public" name="dev_kwg" project="test-single-arch" priority="2">
      <mesa branch="ci_mesa_repo/dev/kwg"/>
    </branch>

    <branch pipeline="public" name="dev_kwg_vulkan" project="test-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/kwg_vulkan"/>
    </branch>

    <branch pipeline="public" name="dev_idr" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/idr/jenkins"/>
    </branch>

    <branch name="mattst88" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/mattst88"/>
    </branch>

    <branch pipeline="public" name="dev_dcbaker" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/dcbaker"/>
    </branch>

    <branch pipeline="public" name="dev_tpalli" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/tpalli/jenkins"/>
    </branch>

    <branch pipeline="public" name="dev_tpalli_vkcts" project="test-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/tpalli/jenkins-vkcts"/>
      <vulkancts branch="internal-github/tpalli_vulkancts"/>
    </branch>

    <branch pipeline="public" name="dev_curro" project="test-single-arch" priority="2">
      <mesa branch="ci_mesa_repo/dev/curro"/>
    </branch>

    <branch pipeline="public" name="dev_curro_vulkan" project="test-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/curro-vk"/>
    </branch>

    <branch pipeline="public" name="dev_nchery_vulkan" project="test-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/nchery_vulkan"/>
    </branch>

    <branch pipeline="public" name="dev_nchery" project="test-single-arch" priority="2">
      <mesa branch="ci_mesa_repo/dev/nchery"/>
    </branch>

    <branch pipeline="public" name="dev_nchery_piglit" project="piglit-gen12" priority="2">
      <piglit branch="nchery-gitlab/dmabuf-files-intel-addition"/>
    </branch>

    <branch pipeline="public" name="dev_tarceri" project="test-single-arch-vulkan" priority="2">
      <mesa branch="tarceri/intel_ci"/>
    </branch>

    <!-- These branches exists to separate build results from
         mesa_master on the server. -->
    <branch name="shader_cache_test" project="test-single-arch"/>
    <branch name="mesa_custom" project="all-test"/>
    <branch name="deqp-runtime_test" project="test-single-arch"/>

    <branch name="imirkin" project="test-single-arch-vulkan" priority="2">
      <mesa branch="imirkin/jenkins"/>
    </branch>

    <branch pipeline="public" name="dev_djdeath_vulkan" project="test-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/djdeath_vulkan"/>
    </branch>

    <branch pipeline="public" name="dev_djdeath" project="test-single-arch-zink" priority="2">
      <mesa branch="ci_mesa_repo/dev/djdeath"/>
    </branch>

    <branch pipeline="public" name="dev_cmarcelo" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/cmarcelo"/>
    </branch>

    <branch pipeline="public" name="dev_cmarcelo_vulkan" project="test-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/cmarcelo_vulkan"/>
    </branch>

    <branch pipeline="public" name="dev_sagarghuge" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/sagarghuge"/>
    </branch>

    <branch pipeline="public" name="dev_sagarghuge_gl" project="test-single-arch" priority="2">
      <mesa branch="ci_mesa_repo/dev/sagarghuge_gl"/>
    </branch>

    <branch pipeline="public" name="dev_sagarghuge_vulkan" project="test-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/sagarghuge_vulkan"/>
    </branch>

    <branch pipeline="public" name="dev_fjdegroo" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/fjdegroo"/>
    </branch>

    <branch pipeline="public" name="dev_global_logic" project="test-single-arch-vulkan" priority="2">
      <mesa branch="global_logic/i965_ci"/>
    </branch>

    <branch pipeline="public" name="dev_global_logic_2" project="test-single-arch-vulkan" priority="2">
      <mesa branch="global_logic/i965_second"/>
    </branch>

    <branch pipeline="public" name="dev_pzanoni" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/pzanoni"/>
    </branch>

    <branch pipeline="public" name="dev_ibriano" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/Sachiel"/>
    </branch>

    <branch pipeline="public" name="dev_jzhang80" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/jzhang80"/>
    </branch>

    <!-- Jobs for this branch are triggered manually -->
    <branch name="internal" project="test-single-arch-internal" priority="2">
    </branch>

    <branch name="anholt" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/anholt"/>
    </branch>

    <branch pipeline="public" name="dev_shadeslayer" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/shadeslayer"/>
    </branch>

    <branch pipeline="public" name="dev_shadeslayer_gl" project="test-single-arch" priority="2">
      <mesa branch="ci_mesa_repo/dev/shadeslayer_gl"/>
    </branch>

    <branch pipeline="public" name="dev_shadeslayer_vulkan" project="test-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/shadeslayer_vulkan"/>
    </branch>

    <branch pipeline="public" name="dev_sushma08" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/sushma08"/>
    </branch>

    <branch pipeline="public" name="dev_janesma" project="all-test-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/janesma"/>
    </branch>

    <branch pipeline="public" name="dev_gfxstrand_gl" project="test-single-arch" priority="2">
      <mesa branch="gfxstrand/jenkins_gl"/>
    </branch>

    <branch pipeline="public" name="dev_gfxstrand_vulkan" project="test-vulkan" priority="2">
      <mesa branch="gfxstrand/jenkins_vulkan"/>
    </branch>

    <branch pipeline="public" name="dev_josouza" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/josouza"/>
    </branch>

    <branch pipeline="public" name="dev_aricursion" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/aricursion"/>
    </branch>

    <branch pipeline="public" name="dev_kaiwenjon" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/kaiwenjon"/>
    </branch>

    <branch pipeline="public" name="dev_xe" project="test-xe" priority="2">
      <mesa branch="ci_mesa_repo/dev/xe"/>
    </branch>

    <branch pipeline="public" name="dev_konstantinseurer" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/konstantinseurer"/>
    </branch>

    <branch pipeline="public" name="dev_zmike" project="test-single-arch-vulkan" priority="2">
      <mesa branch="zmike/intel_ci"/>
    </branch>

    <branch pipeline="public" name="internal_mtl" project="test-single-arch-mtl" priority="2"/>
  </branches>

</build_specification>
