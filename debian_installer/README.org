* Instructions
 - Using the stock debian netinstall iso, navigate to Advanced>Automatic Installation and press 'e' to change the linux commandline.
 - add `netconfig/hostname=otc-gfxtest-{hostname}` at the end of the linux boot line.
 - ctrl-x to continue booting.
 - when prompted for the configuration, enter:  http://otc-mesa-android.jf.intel.com/jenkins/install.cfg
 - Manually partition disks as desired.
