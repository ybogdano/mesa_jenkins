#!/usr/bin/env python3

import os
import pickle
import sys
import tempfile
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from build_support import build
from testers import DeqpTester, DeqpTrie, ConfigFilter
from options import Options
from project_map import ProjectMap
from utils.command import run_batch_command
from utils.utils import (get_libdir, get_libgl_drivers, get_conf_file,
                         get_blacklists, Flakes, write_disabled_tests)


# needed to preserve case in the options
# class CaseConfig(ConfigParser.SafeConfigParser):
#     def optionxform(self, optionstr):
#         return optionstr

class GLESCTSList:
    """provides a deqp lister interface for the deqp runner"""
    def __init__(self):
        self.pmap = ProjectMap()
        self.opt = Options()
        self.flakes = Flakes(self.pmap.current_project(), self.opt)
        self._all_tests = None

    def supports_gles_32(self):
        """indicate whether the target hardware supports opengles 3.2 features"""
        hardware = self.opt.hardware
        if ("bdw" in hardware or
            "bsw" in hardware):
            return False

        # all newer platforms support 3.2
        return True

    def tests(self, env=None):
        """provides all gles cts tests in a trie data structure"""
        if self._all_tests:
            self._all_tests.seek(0)
            return pickle.load(self._all_tests)
        env = {"MESA_GLES_VERSION_OVERRIDE" : "3.2",
               "LD_LIBRARY_PATH" : get_libdir(),
               "MESA_GL_VERSION_OVERRIDE" : "4.6",
               "MESA_GLSL_VERSION_OVERRIDE" : "460",
               "LIBGL_DRIVERS_PATH" : get_libgl_drivers()}
        self.opt.update_env(env)

        savedir = os.getcwd()
        os.chdir(self.pmap.build_root() + "/bin/es/modules")
        run_batch_command(["./glcts", "--deqp-runmode=xml-caselist"],
                          env=env)

        suites = ["KHR-GLES2-cases.xml", "KHR-GLES3-cases.xml", "KHR-GLES31-cases.xml"]

        if self.supports_gles_32():
            suites.append("KHR-GLES32-cases.xml")
            suites.append("KHR-GLESEXT-cases.xml")

        all_tests = DeqpTrie()
        for a_list in suites:
            tmp_trie = DeqpTrie()
            tmp_trie.add_xml(a_list)
            all_tests.merge(tmp_trie)
        os.chdir(savedir)

        self._all_tests = tempfile.TemporaryFile()
        pickle.dump(all_tests, self._all_tests)
        return all_tests

    def blacklist(self, all_tests):
        """remove blacklisted tests from the trie"""
        blacklist_files = get_blacklists()
        blacklist = DeqpTrie()
        for file in blacklist_files:
            blacklist.add_txt(file)
        all_tests.filter(blacklist)

        all_tests.filter(self.flakes.test_list())
        return all_tests

    def flaky(self, _env):
        """return a trie of tests that are flaky on this platform"""
        all_tests = self.tests(_env)
        whitelist = DeqpTrie()
        for a_test in self.flakes.test_list():
            whitelist.add_line(a_test)
        all_tests.filter_whitelist(whitelist)
        return all_tests

class GLESCTSTester:
    """adapts the gles cts binaries to the component interface"""
    def __init__(self):
        self.opt = Options()
        self.pmap = ProjectMap()

    def test(self):
        """run all tests"""
        tester = DeqpTester()
        env = {"MESA_GLES_VERSION_OVERRIDE" : "3.2"}
        if "crocus" in self.opt.hardware:
            env["MESA_LOADER_DRIVER_OVERRIDE"] = "crocus"
        binary = self.pmap.build_root() + "/bin/es/modules/glcts"
        lister = GLESCTSList()
        results = tester.test(binary, lister, env=env)
        tester.test_flaky(binary, lister)
        lister.flakes.generate_xml()

        write_disabled_tests(self.pmap, self.opt, lister.tests(env))
        config = get_conf_file(self.opt.hardware, self.opt.arch,
                               project=self.pmap.current_project())
        tester.generate_results(results, ConfigFilter(config, self.opt))

    def build(self):
        """no build step"""
    def clean(self):
        """no clean step"""

if __name__ == '__main__':
    build(GLESCTSTester())
