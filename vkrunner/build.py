#!/usr/bin/env python3

import os
import shutil
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from build_support import build
from project_map import ProjectMap
from utils.command import run_batch_command
from export import Export

class CargoBuilder:
    def __init__(self):
        self.pm = ProjectMap()
        self.src_dir = self.pm.project_source_dir()

    def build(self):
        pm = self.pm
        src_dir = self.src_dir
        os.makedirs(f"{src_dir}/.cargo", exist_ok=True)
        shutil.copyfile(f"{pm.project_build_dir()}/config", f"{src_dir}/.cargo/config")
        os.chdir(src_dir)
        run_batch_command(["cargo", "install", "--offline",
                           "--path", ".",
                           "--root", pm.build_root()])
        Export().export()

    def clean(self):
        os.chdir(self.src_dir)
        run_batch_command(["cargo", "clean"])
        run_batch_command(["git", "clean", "-xfd"])

    def test(self):
        os.chdir(self.src_dir)
        run_batch_command(["cargo", "test", "--offline", "--", "--test-threads=1"],
                          ignore_fail=True)

if __name__ == '__main__':
    build(CargoBuilder())
